#!/usr/bin/env python3

from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import hashlib
import os
import random

# Create client class
class ChapClient:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.challenge = 0
        self.authenticated = False

# Set config defaults
clientList= list()
clientList.append(ChapClient("MaxMustermann1", "MusterPasswort1"))
clientList.append(ChapClient("MaxMustermann2", "MusterPasswort2"))
clientList.append(ChapClient("MaxMustermann3", "MusterPasswort3"))

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

# Create server
server = SimpleXMLRPCServer(("localhost", 8000),
                            requestHandler=RequestHandler)

# Register pow() function; this will use the value of
# pow.__name__ as the name, which is just 'pow'.
server.register_function(pow)

# Register a function under a different name
def adder_function(x,y):
    return x + y
server.register_function(adder_function, 'add')

# Get client from clientList by username
def clientByName(username):
    for client in clientList:
        if client.username == username:
            return client

# Returns authenticated value of client by clientId
def isAuthenticated(username):
    client = clientByName(username)
    
    if client == None:
        return False
    else:
        return client.authenticated

server.register_introspection_functions()

# Register an instance; all the methods of the instance are
# published as XML-RPC methods (in this case, just 'div').
class MyFuncs:
    def div(self, x, y):
        return x // y
    #Saves and return random int between 10k and 100k
    def challenge(self, username):
        
        client = clientByName(username)

        if client == None:
            print('CHA: ' + username + ' was invalid.')
            return 0

        client.challenge = random.randint(1, 100)
        
        print('CHA: ' + username + ' received challenge \'' + str(client.challenge) + '\'.')
        return client.challenge

    # Compares passwords and authenticates the user
    def authenticate(self, username, clientPasswordHash):

        client = clientByName(username)

        if client == None:
            print('AUTH: ' + username + ' was invalid.')
            return False

        challengePassword = client.password + str(client.challenge)
                
        passwordHash = hashlib.sha512(challengePassword.encode('utf-8')).hexdigest()

        if(passwordHash == clientPasswordHash):
            client.authenticated = True
            print('AUTH: ' + username + ' successfully authenticated.')
        else:
            client.authenticated = False
            print('AUTH: ' + username + ' failed authentication.')

        return client.authenticated

server.register_instance(MyFuncs())

# Run the server's main loop
server.serve_forever()
