Damit das Projekt läuft muss zuerst der Server aufgerufen werden und demnacht der Client, wo dann die Anmeldung erfolgt und nach erfolgreicher CHAP-Authentifizierung werden die RPC-Funktionen aufgerufen.

Anhängigkeiten:
- hashlib
- os
- getpass
- random
- xmlrpc.client
- xmlrpc.server