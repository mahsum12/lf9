#!/usr/bin/env python3

import xmlrpc.client
import getpass
import hashlib
import os

# Connect to Server
s = xmlrpc.client.ServerProxy('http://localhost:8000')

# Prompt user for username and password
username = input('Enter Username: ')
password = getpass.getpass('Enter Password: ')

# Execute challenge request and save returned random int
challenge = s.challenge(username)
challengePassword = password + str(challenge)

# Create hash for password
hashed_password = hashlib.sha512(challengePassword.encode('utf-8')).hexdigest()

# Authenticate at the server
authenticated = s.authenticate(username, hashed_password)

# If authentification was successfull than run server-functions
if authenticated:
    print('AUTHENTIFICATION SUCCESSFULL')
    print(s.pow(2,3))  # Returns 2**3 = 8
    print(s.add(2,3))  # Returns 5
    print(s.div(5,2))  # Returns 5//2 = 2

    # Print list of available methods
    print(s.system.listMethods())

# Print that the authentification failed
else:
    print('AUTHENTIFICATION FAILED')


